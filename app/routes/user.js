var Answer = require('../models/answer');
var User = require('../models/user');
var Question = require('../models/question');
var Tag = require('../models/tag');
var Badge = require('../models/badge');
var Favorite = require('../models/favorite');
var QuestionTag = require('../models/questiontag');
var Vote = require('../models/vote');
var nodemailer = require('nodemailer');
var randtoken = require('rand-token');
var async = require('async');
var crypto = require('crypto');

// create reusable transporter object using SMTP transport
//var transporter = nodemailer.createTransport("SMTP", {
var transporter = nodemailer.createTransport({
    service: 'Zoho',
    auth: {
        user: 'govtalk@zoho.com',
        pass: 'Keyon123'
    },
    tls: {
        rejectUnauthorized: false
    },
        debug:true
});

var fs = require('fs');

module.exports = function (app, passport) {

    app.post('/api/user/upload/avatar',function(req, res) {
      var fstream;
        req.pipe(req.busboy);
        req.busboy.on('file', function (fieldname, file, filename) {
            console.log("Uploading: " + filename);
            var extension=(/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
            var strand= randtoken.generate(20);
            fstream = fs.createWriteStream('public/uploads/users/'+strand+'.'+ extension);
            file.pipe(fstream);
            fstream.on('close', function (err) {
                if(err)
                    res.send(err);
                res.send(strand+'.'+extension);
            });
        });
    });
    app.get('/api/user/edit/avatar/:avatar', function(req, res){
        User.findById(req.user._id,function(err, user){
            if(err)
                res.send(err);
            user.avatar="/uploads/users/"+req.params.avatar;
            user.save(function(err, user){
                if(err)
                    res.send(err);
                res.json(user);
            });
        });
    });
    app.get('/api/user', function(req, res) {
        User.find(function(err, user){
            if (err)
                res.send(err);
            res.json(user);
        });
     });
    // Get User Admin
    app.get('/api/admin',function(req,res)
    {
        User.find({role:'admin'}).select('_id').exec(function(err,user)
        {
            if(err)
                res.send(err);
            res.json(user);
        });
    });

    app.get('/api/people', function(req, res) {
        User.find({},'-_id displayName',function(err, user){
            if (err)
                res.send(err);
            res.json(user);
        });
     });
    app.get('/api/countUser', function(req, res) {
        User.find({status: 1}).count(function(err, user){
            if (err)
                res.send(err);
            res.json(user);
        });
     });
    app.get('/api/user/count/question/:user_id', function(req, res) {
        var id= req.params.user_id;
        Question.count({userId:id},function(err, c){
            if (err)
                res.send(err);
            res.json(c);
        });
     });
    app.get('/api/user/count/answer/:user_id', function(req, res) {
        var id= req.params.user_id;
        Answer.count({userId:id},function(err, c){
            if (err)
                res.send(err);
            res.json(c);
        });
     });
    app.get('/api/user/profile/:user_id', function(req, res) {
        User.findById(req.params.user_id,function(err, user){
            if (err)
                res.send(err);
            res.json(user);
        });
     });
    app.delete('/api/user/delete/:user_id', function(req, res){
        var id= req.params.user_id;
        if(id==req.user._id){
            res.send({"error_msg":"Error . You can not remove yourself."});
        }
        else{
            //Find all the questions posted by this member
            Question.find({userId: id}, function(err, questions){
                if(err)
                    res.send(err);
                questions.forEach(function(item){
                    QuestionTag.remove({questionId: item._id}, function(err, tags){
                        if(err)
                            res.send(err);
                    });
                    Answer.find({questionId: item._id}, function(err, answers){
                        answers.forEach(function(i){
                            Vote.remove({answerId: i._id}, function(err){
                                if(err)
                                    res.send(err);
                            });
                        });
                    });
                    //Delete all the answers in this question
                    Answer.remove({questionId: item._id}, function(err, answers){
                        if(err)
                            res.send(err);
                    });
                    Vote.remove({questionId: item._id}, function(err, vote){
                        if(err)
                            res.send(err);
                    });
                    Favorite.remove({questionId: item._id}, function(err, favorite){
                        if(err)
                            res.send(err);
                    });
                });
            });
            //After removing all the links to that question , then delete it
            Question.remove({userId: id}, function(err, questions){
                if(err)
                    res.send(err);
            });

            //For the answer of this user and delete all related vote
            Answer.find({userId: id}, function(err, answers){
                if(err)
                    res.send(err);
                answers.forEach(function(item){
                    Vote.remove({answerId: item._id}, function(err,a){
                        if(err)
                            res.send(err);
                    });
                });
            });
            //Delete all the answers this member
            Answer.remove({
                userId: id
            }, function(err, answers){
                if(err)
                    res.send(err);
            });
            //Clear evaluation
            Vote.remove({
                userId: id
            }, function(err, votes){
                if(err)
                    res.send(err);
            });
            //Clear favorites
            Favorite.remove({userId: id}, function(err, favorites){
                if(err)
                    res.send(err);
            });
            User.remove({
                _id: id
            }, function(err, users){
                if(err)
                    res.send(err);
                User.find(function(err, users) {
                    if (err)
                        res.send(err)
                    res.json(users);
                });
            });
        }
    });
    app.get('/api/user/favorite', function(req, res){
        Favorite.find({userId: req.user._id}, function(err, list){
            if(err)
                res.send(err);
            res.json(list);
        });
    });
    app.get('/api/user/favorite/all', function(req, res){
        Favorite.find({}, function(err, list){
            if(err)
                res.send(err);
            res.json(list);
        });
    });
    app.get('/api/user/vote', function(req, res){
        Vote.find({userId: req.user._id}, function(err, list){
            if(err)
                res.send(err);
            res.json(list);
        });
    });
    app.get('/api/user/vote/all', function(req, res){
        Vote.find({}, function(err, list){
            if(err)
                res.send(err);
            res.json(list);
        });
    });
    app.get('/api/user/active/:user_id/:token', function(req, res){
        User.findById(req.params.user_id,function(err, user){
            if (err)
                res.send(err);
            var token = req.params.token;
            if(user.activeToken==token)
                user.status=1;
            user.save(function(err, u){
                if(err)
                    res.send(err);
                res.json(u);
            });
        });
    });
    app.post('/api/user/getUserbyEmail', function(req, res){
        User.find({email: req.body.email}, function(err, user){
            if(err)
                res.send(err);
            res.send(user);
        });
    });
    app.post('/api/user/edit', function(req, res){
        User.findById(req.body._id, function(err, user){
            if(err)
                res.send(err);
            user.displayName=req.body.displayName;
            user.location=req.body.location;
            user.website = req.body.website;
            user.birthday = req.body.birthday;
            user.save(function(err, u){
                if(err)
                    res.send(err);
                res.json(u);
            });
        });
    });
    app.post('/api/user/changePassword', function(req, res, done){
        User.findById(req.body._id, function(err, user){
            if(err)
                res.send(err);
            if (!user || !user.validPassword(req.body.CurrentPassword))
                return done(null, false);
            else{
                user.password =user.generateHash(req.body.NewPassword);
                user.lastEditDate=new Date();
                user.save(function(err, u){
                    if(err)
                        res.send(err);
                    res.json(u);
                });
            }
        });
    });
    app.post('/forgot', function(req,res){
        async.waterfall([
            function(done) {
              crypto.randomBytes(20, function(err, buf) {
                var token = buf.toString('hex');
                done(err, token);
              });
            },
            function(token, done) {
              User.findOne({ email: req.body.email }, function(err, user) {
                if (!user) {
                  req.flash('error', 'No account with that email address exists.');
                  return res.redirect('/forgot');
                }

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 30000; // calculated expiration time ms 3600000

                user.save(function(err) {
                  done(err, token, user);
                });
              });
            },
            function(token, user, done) {
                var domain =req.headers.host || "govtalk@zoho.com";
              var mailOptions = {
                to: user.email,
                from: 'No-Reply @ GovTalk <govtalk@zoho.com>',
                subject: 'Email password recovery',
                text: 'You are receiving this email because you ( or someone ) has requested to change your account password.\n\n' + 'Please click on the link below or copy and paste into the address box of your browser Sign in to complete treatment:\n\n' + 'http://' + domain + '/users/reset-password/' + token + '\n\n' +'If not you require changes to your account, simply ignore this email .\n'+'Note that this email is only valid within 1 hour from the time the request processor.'
              };
              transporter.sendMail(mailOptions, function(err) {
                done(err, 'done');
              });
            }
          ], function(err) {
            if (err) return next(err);
            res.redirect('/forgot');
          });
    });

    app.post('/api/user/resetPassword', function(req, res) {
      User.findOne({ resetPasswordToken: req.body.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          res.send({"error_msg":"Password recovery code is invalid or has expired. Please request another code!"});
        }
        else{
            user.password = user.generateHash(req.body.NewPassword);
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            user.save(function(err) {
                var mailOptions = {
                to: user.email,
                from: 'No-Reply @ GovTalk <govtalk@zoho.com>',
                subject: 'Password has been changed',
                text: 'This is the confirmation e-mail account passwords '+user.displayName+' has been changed.'
              };
              transporter.sendMail(mailOptions, function(err, info) {
                if(err){
                    console.log(err);
                }else{
                    console.log('Message sent: ' + info.response);
                }
              });
              req.logIn(user, function(err) {
                res.json(user);
              });
            });
        }
    });
  });
    app.post('/api/user/updatePermission', function(req, res){
        User.findById(req.body._id, function(err, user){
            if(err)
                res.send(err);
            user.role=req.body.role;
            user.lastEditDate=new Date();
            user.save(function(err, u){
                if(err)
                    res.send(err);
                res.json(user);
            });
        });
    });
    app.post('/api/user/upload',function(req, res) {
      var fstream;
        req.pipe(req.busboy);
        req.busboy.on('file', function (fieldname, file, filename) {
            console.log("Uploading: " + filename);
            var extension=(/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
            var strand= randtoken.generate(20);
            fstream = fs.createWriteStream('public/uploads/images/'+strand+'.'+ extension); // creates a writable stream
            file.pipe(fstream);
            fstream.on('close', function (err) {
                if(err)
                    res.send(err);
                res.send(strand+'.'+extension);
            });
        });
    });

    app.get('/api/user/online/:user_id', function(req, res){
        var id= req.params.user_id;
        User.findById(id, function(err, user){
            if(err)
                res.send(err);
            user.online="true";
            user.save(function(err, u){
                if(err)
                    res.send(err);
                res.json(user);
            });
        });
    });
    app.get('/api/user/offline/:user_id', function(req, res){
        var id= req.params.user_id;
        User.findById(id, function(err, user){
            if(err)
                res.send(err);
            user.online="false";
            user.save(function(err, u){
                if(err)
                    res.send(err);
                res.json(user);
            });
        });
    });


}
