var Answer = require('../models/answer');
var User = require('../models/user');
var Question = require('../models/question');
var Tag = require('../models/tag');
var QuestionTag = require('../models/questiontag');
var Favorite = require('../models/favorite');
var Vote = require('../models/vote');
var Report = require('../models/report');
var nodemailer = require('nodemailer');
var randtoken = require('rand-token');

// create reusable transporter object using SMTP transport
//var transporter = nodemailer.createTransport("SMTP", {
var transporter = nodemailer.createTransport({
    service: 'Zoho',
    auth: {
        user: 'govtalk@zoho.com',
        pass: 'Keyon123'
    }
});


module.exports = function (app, passport) {
//Get the list of question
    app.get('/api/question',function(req, res) {
        Question.find({status:true}).populate('userId').exec(function(err, questions){
            if (err)
                res.send(err)
            res.json(questions);
        });
     });
    app.get('/api/question/all', function(req, res){
        Question.find({}).populate('userId').exec(function(err, questions){
            if (err)
                res.send(err)
            res.json(questions);
        });
    });
    // Search Title
    app.get('/api/title/search/:title',function(req,res)
    {
        var title=req.params.title;
        Question.find({ $text :{"$search":title}},{score:{$meta:"textScore"}}).sort({score:{$meta:"textScore"}}).exec(function(err,questions)
        {
            if(err)
                res.send(err)
            res.json(questions);
        });
    });
    app.get('/api/question/count', function(req, res) {
        Question.count(function(err, question){
            if (err)
                res.send(err)
            res.json(question);
        });
     });
    //Get featured dataset - show on the homepage
    app.get('/api/question/popular', function(req, res) {
        Question.find({status:true}).where('creationDate').gt(new Date()-3600*1000*24*7).populate({path: 'userId', select: 'avatar',options: { limit: 5 }})
        .select('title creationDate userId')
        .sort('-score')
        .exec(function(err, questions){
            if (err)
                res.send(err)
            res.json(questions);
        });
     });
    //Manage dataset
    app.post('/api/question/create', function(req, res) {
        // Add new dataset.
        var newQuestion= new Question();
        newQuestion.title = req.body.title;
        newQuestion.content = req.body.content;
        newQuestion.userId=req.user._id;

        var listTag = req.body.tag;
        newQuestion.creationDate= new Date();
        newQuestion.activeToken = randtoken.generate(60);
        if(req.user.reputation>=10 || req.user.role=="admin"){
            newQuestion.status=true;
            //Plus 2 points for members post immediately if there is an official member
            User.findById(req.user._id, function(err, u){
                if(err)
                    res.send(err);
                u.reputation +=2;
                u.save(function(err, u){
                    if(err)
                        res.send(err);
                });
            });
            //If you are not yet full members will earn points after the dataset is accepted
        }
        else
            newQuestion.status=false;
        newQuestion.save(function(err, question) {
            if (err)
                throw err;
            if(question.status==false){
                User.find({role:"admin"}).select('email').exec(function(err, user){
                    if(err)
                        res.send(err);
                    var domain =req.headers.host || "govtalk@zoho.com";
                    var mailOptions = {
                        from: 'No-Reply @ GovTalk <govtalk@zoho.com>', //Return address
                        to: user, //List of recipients, separated by commas
                        subject: 'New question is posted', //Subject Line
                        //text: 'Hello world', //Contents of letter forms often
                        html: '<p><strong>Hi administrator.</strong></p> <p>There is an question with the title"<strong>'+question.title+'</strong>" recently published in &nbsp;govtalk.com by <strong>'+req.user.displayName+'</strong>.'
                        +' Please visit http://' + domain +'/system/questions to examine and approve this post .</p>' // Html content
                    };

                    // send mail with subject transporter was declared
                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
                            console.log(error);
                        }else{
                            console.log('Message sent: ' + info.response);
                        }
                    });
                });
            }
            //Add the tag linked to the table QuestionTag
            listTag.forEach(function(item){
                Tag.find({tagName: item.tagName}).exec(function(err,t){
                    if(err)
                        res.send(err);
                    t.forEach(function(item){
                        var tag = new QuestionTag();
                        tag.questionId=question._id;
                        tag.tagId=item._id;
                        tag.save(function(err){
                            if(err)
                                throw err;
                        });
                    });

                    //Update number of uses of the tag by 1 unit
                    t.forEach(function(item){
                        Tag.findById(item._id, function(err, tag){
                            if(err)
                                res.send(err);
                            tag.count +=1;
                            tag.save(function(err, tag){
                                if(err)
                                    res.send(err);
                            });
                        });
                    });
                });
            });
            res.json(question);
        });
    });
    //Detailed questions
    app.get('/api/question/detail/:question_id', function(req, res){
        var id=req.params.question_id;
        Question.findById(id).populate('userId').exec(function(err, question){
            if (err)
                res.send(err)
            res.json(question);
        });
    });
    app.get('/api/findAnswers/:question_id', function(req, res) {
        var id=req.params.question_id;
        Question.findAnswers(id).populate('userId').exec(function(err,answer){
            res.json(answer)
        });
     });
    //Get the id tag under question
    app.get('/api/findTags/:question_id', function(req, res) {
        var id=req.params.question_id;
        Question.findTags(id).populate('tagId').exec(function(err,tag){
            res.json(tag)
        });
     });
    //Delete questions
    app.delete('/api/question/detete/:question_id', function(req, res) {
        var id = req.params.question_id;

        //Subtract 2 points on the score of a member to post
        Question.findById(id).populate({path:'userId', select:'_id'}).exec(function(err, q){
            if(q.status==true){
                User.findById(q.userId._id, function(err, u){
                    if(err)
                        res.send(err);
                    if(u.reputation>=2)
                        u.reputation -=2;
                    u.save(function(err, u){
                        if(err)
                            res.send(err);
                    });
                });
            }
        });
        //Reduce the amount of time using the tag in question down 1 unit
        Question.findTags(id).populate({path:'tagId', select:"_id"}).exec(function(err, t){
            if(err)
                res.send(err);
            t.forEach(function(item){
                Tag.findById(item.tagId, function(err, tag){
                        if(err)
                            res.send(err);
                        if(tag.count>0)
                            tag.count -=1;
                        tag.save(function(err, tag){
                            if(err)
                                res.send(err);
                        });
                    });
            });
        });
        //Remove the link tag before deleting questions
        QuestionTag.remove({
            questionId: id
        }, function(err, tags){
            if(err)
                res.send(err);
        });
        //Delete all the answers in this question
        Answer.remove({
            questionId: id
        }, function(err, answers){
            if(err)
                res.send(err);
        });
        Vote.remove({questionId: id}, function(err, vote){
            if(err)
                res.send(err);
        });
        Favorite.remove({questionId: id}, function(err, favorite){
            if(err)
                res.send(err);
        });

        Question.remove({
            _id : id
        }, function(err, questions) {
            if (err)
                res.send(err);

            // Returns a list of new questions .
            Question.find({}).populate("userId").exec(function(err, questions) {
                if (err)
                    res.send(err)
                res.json(questions);
            });
        });
    });

    //Lấy toàn bộ giá trị trong questionTag
    app.get('/api/questiontag/getall', function(req, res){
        QuestionTag.find({}).populate('tagId').exec(function(err, tags){
            if (err)
                res.send(err)
            res.json(tags);
        });
    });
    //Like dataset
    app.get('/api/question/vote_up/:question_id', function(req, res){
        var id = req.params.question_id;
        Vote.findOne( { $and: [ { questionId: id }, { userId: req.user._id } ] } )
        .exec(function(err,data){
            if(err)
                res.send(err);
            if(data!=null){
                if(data.type==true){
                    Vote.remove({_id: data._id}, function(err, d){
                        if(err)
                            res.send(err);
                        Question.findById(id, function(err, question){
                            if(err)
                                res.send(err);
                            question.score-=1;
                            question.save(function(err, q){
                                if(err)
                                    res.send(err);
                            });
                        });
                        res.json(d);
                    });
                }
                else{
                    Vote.remove({_id: data._id}, function(err, d){
                        if(err)
                            res.send(err);
                        var vote = new Vote();
                        vote.questionId=id;
                        vote.userId= req.user._id;
                        vote.type=true;
                        vote.creationDate = new Date();
                        vote.save(function(err, v){
                            if(err)
                                res.send(err);
                            Question.findById(id, function(err, question){
                                if(err)
                                    res.send(err);
                                question.score+=2;
                                question.save(function(err, q){
                                    if(err)
                                        res.send(err);
                                    res.json(q);
                                });
                            });
                        });
                    });
                }
            }
            else{
                var vote = new Vote();
                vote.questionId=id;
                vote.userId= req.user._id;
                vote.type=true;
                vote.creationDate = new Date();
                vote.save(function(err, v){
                    if(err)
                        res.send(err);
                    Question.findById(id, function(err, question){
                        if(err)
                            res.send(err);
                        question.score+=1;
                        question.save(function(err, q){
                            if(err)
                                res.send(err);
                            res.json(q);
                        });
                    });
                });
            }
        });
    });
    //Dislikes or likes
    app.get('/api/question/vote_down/:question_id', function(req, res){
        var id = req.params.question_id;
        Vote.findOne( { $and: [ { questionId: id }, { userId: req.user._id } ] } )
        .exec(function(err,data){
            if(err)
                res.send(err);
            if(data!=null){
                if(data.type==false){
                    Vote.remove({_id: data._id}, function(err, d){
                        if(err)
                            res.send(err);
                        Question.findById(id, function(err, question){
                            if(err)
                                res.send(err);
                            question.score+=1;
                            question.save(function(err, q){
                                if(err)
                                    res.send(err);
                            });
                        });
                        res.json(d);
                    });
                }
                else{
                    Vote.remove({_id: data._id}, function(err, d){
                        if(err)
                            res.send(err);
                        var vote = new Vote();
                        vote.questionId=id;
                        vote.userId= req.user._id;
                        vote.type=false;
                        vote.creationDate = new Date();
                        vote.save(function(err, v){
                            if(err)
                                res.send(err);
                            Question.findById(id, function(err, question){
                                if(err)
                                    res.send(err);
                                question.score-=2;
                                question.save(function(err, q){
                                    if(err)
                                        res.send(err);
                                    res.json(q);
                                });
                            });
                        });
                    });
                }
            }
            else{
                var vote = new Vote();
                vote.questionId=id;
                vote.userId= req.user._id;
                vote.type=false;
                vote.creationDate = new Date();
                vote.save(function(err, v){
                    if(err)
                        res.send(err);
                    Question.findById(id, function(err, question){
                        if(err)
                            res.send(err);
                        question.score-=1;
                        question.save(function(err, q){
                            if(err)
                                res.send(err);
                            res.json(q);
                        });
                    });
                });
            }
        });
    });
    //favorite questions
    app.get('/api/question/favorite/:question_id', function(req, res){
        var id = req.params.question_id;
        Favorite.findOne( { $and: [ { questionId: id }, { userId: req.user._id } ] } )
        .exec(function(err,data){
            if(err)
                res.send(err);
            //res.json(data);
            if(data!=null){
                Favorite.remove({_id: data._id}, function(err, d){
                    if(err)
                        res.send(err);
                    res.json(d);
                });
            }
            else{
                var favorite = new Favorite();
                favorite.questionId=id;
                favorite.userId= req.user._id;
                favorite.save(function(err, f){
                    if(err)
                        res.send(err);
                    res.json(f);
                });
            }
        });
    });
    // Get the user favoorite questions
    app.get('/api/findFavorite/:question_id', function(req, res) {
        var id=req.params.question_id;
        Favorite.find({questionId:id}).populate('userId').exec(function(err,favorite){
            res.json(favorite)
        });
     });
    app.post('/api/question/edit', function(req, res) {

        Question.findById(req.body._id).populate("userId").exec(function(err, question){
            question.title = req.body.title;
            question.content = req.body.content;
            question.lastEditDate= new Date();
            var listTag = req.body.tag;
            question.save(function(err, question) {
                if (err)
                    throw err;
                if(listTag && listTag.length!=0){
                    Question.findTags(req.body._id).populate('tagId').exec(function(err,data){
                        //Reduce the amount of time using the tag in question
                        data.forEach(function(item){
                            Tag.findOne({tagName: item.tagId.tagName}).exec(function(err,tag){
                                if(err)
                                    res.send(err);
                                //Update number of uses of the tag down 1 unit
                                if(tag.count>0)
                                    tag.count -=1;
                                tag.save(function(err, tag){
                                    if(err)
                                        res.send(err);
                                });
                            });
                        });
                    });
                    // Remove all old tags linked to this question .
                   QuestionTag.remove({questionId: question._id}, function(err, q){
                        if(err)
                            res.send(err);
                    });
                    //Add the tag linked to the table QuestionTag
                    listTag.forEach(function(listitem){
                        Tag.find({tagName: listitem.tagName}).exec(function(err,t){
                            if(err)
                                res.send(err);
                            t.forEach(function(item){
                                var tag = new QuestionTag();
                                tag.questionId=question._id;
                                tag.tagId=item._id;
                                tag.save(function(err){
                                    if(err)
                                        throw err;
                                });
                            });

                            //Update number of uses of the tag by 1 unit
                            t.forEach(function(item){
                                Tag.findById(item._id, function(err, tag){
                                    if(err)
                                        res.send(err);
                                    tag.count +=1;
                                    tag.save(function(err, tag){
                                        if(err)
                                            res.send(err);
                                    });
                                });
                            });
                        });
                    });
                }
                res.json(question);
            });
        });
    });
    app.get('/api/question/approve/:question_id', function(req, res){
        Question.findById(req.params.question_id).populate("userId").exec(function(err, question){
            question.status=true;
            question.save(function(err,q){
                if(err)
                    res.send(err);
                var mailOptions = {
                    from: 'No-Reply @ GovTalk <govtalk@zoho.com>', // Return address
                    to: question.userId.email, //List of recipients , separated by commas
                    subject: 'The article was approved', // Subject Line
                    //text: 'Hello world', 
                    html: '<p><strong>Hello '+question.userId.displayName+'.</strong></p> <p>The question"<strong>'+question.title+'</strong>"Your question have been posted. Lets start discussing the topic you have raised.</p>'
                    // Html contentHtml
                };

                // send mail with subject transporter was declared
                transporter.sendMail(mailOptions, function(error, info){
                    if(error){
                        console.log(error);
                    }else{
                        console.log('Message sent: ' + info.response);
                    }
                });
                //Points added to members after post is approved
                User.findById(question.userId._id, function(err, u){
                    if(err)
                        res.send(err);
                    u.reputation +=2;
                    u.save(function(err, u){
                        if(err)
                            res.send(err);
                    });
                });
                Question.find({}).populate('userId').exec(function(err, questions){
                    if (err)
                        res.send(err)
                    res.json(questions);
                });
            });

        });
    });
    app.get('/api/question/getQuestionByUser/:user_id', function(req, res){
        Question.find({userId: req.params.user_id}).populate("userId").exec(function(err, list){
            if(err)
                res.send(err);
            res.json(list);
        });
    });
    app.get('/api/question/report/:question_id', function(req, res,done) {
        var id = req.params.question_id;
        Report.findOne( { $and: [ { questionId: id }, { userReported: req.user._id } ] } ).exec(function(err,data){
            if(err)
                res.send(err);
            res.json(data);
        });

    });
    app.post('/api/question/report/create', function(req, res,done) {
        Report.create({
            userReported : req.user._id,
            userId: req.body.userId,
            questionId : req.body.questionId,
            content: req.body.content,
            type: 'question',
            creationDate: new Date(),
        }, function(err, report) {
            if (err)
                res.send(err);
            res.json(report);
        });

    });
}
