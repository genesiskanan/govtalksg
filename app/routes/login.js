var User = require('../models/user');
var nodemailer = require('nodemailer');

// create reusable transporter object using SMTP transport
//var transporter = nodemailer.createTransport("SMTP", {
var transporter = nodemailer.createTransport({
    service: 'Zoho',
    auth: {
        user: 'govtalk@zoho.com',
        pass: 'Keyon123'
    }
});

module.exports = function (app, passport) {
    //Manage members
    app.get('/loggedin', function(req, res) { res.send(req.isAuthenticated() ? req.user : '0'); });
     // handles the login page
    app.post('/login', passport.authenticate('login'), function(req, res) {
      res.send(req.user);
    });

    console.log('created');
    // handling registration page
    app.post('/signup', passport.authenticate('signup'), function(req, res) {
        /*Send email successfully after the accounts are created*/
        // Establish the content of messages
        var domain =req.headers.host || "govtalk@zoho.com";
        var mailOptions = {
            from: 'No-Reply @ GovTalk <govtalk@zoho.com>', // Return address
            to: req.user.email, //List of recipients , separated by commas
            subject: 'Email activation', // Subject Line
            html: '<strong>Hi '+req.user.displayName+' you have successfully registered accounts at One Map 2.0.</strong><br><p>Registration Information</p><ul><li>Email: '+req.user.email+'</li><li>Display Name: '+req.user.displayName+'</li><li>Password: ******</li></ul><br /><p>Please activate your account by clicking &nbsp;<a href="http://' + domain + '/users/active/'+req.user._id+'/'+req.user.activeToken+'"target="_blank">here</a>' // Nội dung dạng html

        };

        // send mail with subject transporter was declared
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }else{
                console.log('Message sent: ' + info.response);
            }
        });
        res.send(req.user);
    });
    // Log out
    app.get('/logout', function(req, res) {
            req.logout();
            req.session.destroy();
            res.send(200);
    });
    //Handle login facebook

    // send to facebook to require authentication
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

    //handle callback after use facebook authenticated
    app.get('/auth/facebook/callback',
            passport.authenticate('facebook', {
                    successRedirect : '/',
                    failureRedirect : '/login'
            }));


    // Handling account login via google

    // send to facebook to require authentication
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    // handle callback after the authenticated user google
    app.get('/auth/google/callback',
            passport.authenticate('google', {
                    successRedirect : '/',
                    failureRedirect : '/login'
            }));
}
