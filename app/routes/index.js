var question = require('./question');
var user = require('./user');
var answer = require('./answer');
var login = require('./login');
var tag = require('./tag');
var badge = require('./badge');
var system = require('./system');
var chat = require('./chat');
var notifi = require('./notifi');
var report = require('./report');
/*var CronJob = require('cron').CronJob;
try {
     new CronJob('00 13 17 * * * *', function() {
     console.log('You will see this message every 17:06 in Vietnam');
    }, function () {
    },
    true
)} catch(ex) {
        console.log("cron pattern not valid");
}*/



var mongoose = require('mongoose');

module.exports = function (app, passport) {

    // Manage questions
    question(app);
    //Manage members
    user(app);
    //Management responses
    answer(app);
    //To manage the registration , login , logout of the members .
    login(app,passport);
    //Management tag
    tag(app);
    //Manage badge
    badge(app);
    //Management of information on system configuration
    system(app);
    chat(app);
    notifi(app);
    report(app);


    //All requests must go through index.html page for processing.
    app.get('*', function(req, res){
        res.sendfile('public/index.html');
    });
};
