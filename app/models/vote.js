var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;


// create db structure
var schema = mongoose.Schema({
   questionId:      {type: ObjectId, ref: 'Question' },
   userId:          {type: ObjectId, ref: 'User' },
   answerId:        {type: ObjectId, ref: 'Answer'},
   type:            {type:'Boolean', required: true},
   creationDate:    {type: 'Date', default: Date.now}
});
// create and export the model for Favorite on app
module.exports = mongoose.model('Vote', schema);
