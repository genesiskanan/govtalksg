var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// create db structure
var schema = mongoose.Schema({
    displayName:   {type: 'String', required: true},
    email:         {type: 'String', required: true},
    password:      {type: 'String', default: null},
    avatar:        {type: 'String', default: null},
    location:      {type: 'String', default: null},
    website:       {type: 'String', default: null},
    birthday:      {type: 'String', default: null},
    reputation:    {type: 'Number', default: 0},
    status:        {type: 'Number', default: 0},
    role:          {type: 'String',required:true},
    activeToken :  {type: 'String', default: null},
    resetPasswordToken: {type: 'String', default: null},
    resetPasswordExpires: {type: 'String', default: Date.now},
    creationDate:  {type: 'Date', default: Date.now},
    lastEditDate:  {type: 'Date', default: Date.now},
    lastAccessDate:{type: 'Date', default: Date.now}

});

// create hash
schema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// check valid password
schema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

// create and export the model to the user in the app
module.exports = mongoose.model('User', schema);
