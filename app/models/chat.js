var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;


// create db structure
var schema = mongoose.Schema({
   userSend:        {type: ObjectId, ref: 'User' },
   userRecive:      {type: ObjectId, ref: 'User' },
   msg:             {type: 'String', require: true},
   status:          {type: 'Boolean', default: false},
   creationDate:    {type: 'Date', default: Date.now}
});
// create and export the model for chat on app
module.exports = mongoose.model('Chat', schema);
