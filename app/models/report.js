var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;


// create db structure
var schema = mongoose.Schema({
   userReported:    {type: ObjectId, ref: 'User' },
   userId:          {type: ObjectId, ref: 'User' },
   questionId:      {type: ObjectId, ref: 'Question'},
   answerId:        {type: ObjectId, ref: 'Answer'},
   type:            {type:'String', required: true},
   content:         {type: 'String', default: null},
   status:          {type: 'Boolean', default:false},
   creationDate:    {type: 'Date', default: Date.now}
});
// create and export the model to the Report on the app
module.exports = mongoose.model('Report', schema);
