var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
// create db structure
var schema = mongoose.Schema({
   questionId:      {type: ObjectId, ref: 'Question' },
   tagId:           {type: ObjectId, ref: 'Tag' }

});


// create and export the model to QuestionTag in app
module.exports = mongoose.model('QuestionTag', schema);
