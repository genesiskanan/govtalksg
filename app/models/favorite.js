var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;


// create db structure
var schema = mongoose.Schema({
   questionId:      {type: ObjectId, ref: 'Question' },
   userId:          {type: ObjectId, ref: 'User' },
   creationDate:    {type: 'Date', default: Date.now}
});

module.exports = mongoose.model('Favorite', schema);
