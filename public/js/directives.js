angular.module('appDirectives', [])
/*Compare password*/
.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope:true,
        require: 'ngModel',
        link: function (scope, elem , attrs,control) {
            var checker = function () {

                /*take the value of the password*/
                var e1 = scope.$eval(attrs.ngModel);

                /*take the value of the password confirmation*/
                var e2 = scope.$eval(attrs.passwordMatch);
                return e1 == e2;
            };
            scope.$watch(checker, function (n) {

                /*establish control form*/
                control.$setValidity("unique", n);
            });
        }
    };
}])
.directive('enforceMaxTags', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngCtrl) {
      var maxTags = attrs.maxTags ? parseInt(attrs.maxTags, '10') : null;

      ngCtrl.$parsers.push(function(value) {
        if (value && maxTags && value.length > maxTags) {
          value.splice(value.length - 1, 1);
        }
        return value;
      });
    }
  };
})
/*Fixed vertical sliding menu*/
.directive('wayPoints', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            if( $(element).length > 0 ){
                    $(element).waypoint(function(direction) {
                       if(direction == "down")
                          $('.fixed-menu').fadeIn();
                       else
                          $('.fixed-menu').fadeOut('fast');

                  }, { offset: 0 });
            }
        }
    };
});
