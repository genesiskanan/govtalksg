angular.module('appRoutes',[]).config(['$stateProvider', '$urlRouterProvider','$locationProvider',
 function($stateProvider, $urlRouterProvider,$locationProvider){
 /*Navigate 404*/
  $urlRouterProvider.otherwise("/404.html");
  $urlRouterProvider.when('/_=_', '/');
  /*Establish state*/
  $stateProvider
    /*===============CREATEMYMAP================*/
    .state('home', {
      url: "/",
      templateUrl: "views/public/home.html",
      controller: 'ListQuestionController',
      title: 'Home',
      access: { requiredLogin: false },
    })
    .state('journeyPlanner', {
      url: "/journey-planner.html",
      templateUrl: "views/public/journeyPlanner.html",
      controller: 'SystemController',
      title: 'Journey Planner',
      access: { requiredLogin: false },
    })
    .state('schoolQuery', {
      url: "/school-query.html",
      templateUrl: "views/public/schoolQuery.html",
      controller: 'SystemController',
      title: 'School Query',
      access: { requiredLogin: false },
    })
    .state('landQuery', {
      url: "/land-query.html",
      templateUrl: "views/public/landQuery.html",
      controller: 'SystemController',
      title: 'Land Query',
      access: { requiredLogin: false },
    })
    .state('question-detail', {
        url: "/cau-hoi/chi-tiet/:id/:slug",
        templateUrl : 'views/public/question/detail.html',
        controller :'DetailQuestionController',
        title: 'Detailed questions',
        access: { requiredLogin: false }
    })
    .state('system-question',{
        url: "/system/questions",
        templateUrl : 'views/private/question/manage.html',
        controller :'ListQuestionController',
        title: 'Manage questions',
        access: { requiredLogin: true }
    })
    .state('system-question-edit',{
        url: "/system/questions/edit/:id",
        templateUrl : 'views/private/question/edit.html',
        controller :'QuestionController',
        title: 'Edit System Questions',
        access: { requiredLogin: true }
    })
    /*===============ANSWERS================*/
    .state('system-answer',{
        url: "/system/answers",
        templateUrl : 'views/private/answer/manage.html',
        controller :'AnswerController',
        title: 'Manage answer',
        access: { requiredLogin: true }
    })
    .state('system-answer-detail',{
        url: "/system/answers/detail/:id",
        templateUrl : 'views/private/answer/detail.html',
        controller :'AnswerController',
        title: 'Manage answer deatil',
        access: { requiredLogin: true }
    })
    /*===============BADGES================*/
    .state('ranking',{
        url: "/ranking.html",
        templateUrl : 'views/public/ranking.html',
        controller :'BadgeController',
        title: 'Ranking',
        access: { requiredLogin: false }
    })
    .state('system-badge',{
        url: "/system/badges",
        templateUrl : 'views/private/badge/manage.html',
        controller :'BadgeController',
        title: 'Manage title',
        access: { requiredLogin: true }
    })
    .state('system-badge-add',{
        url: "/system/badges/add",
        templateUrl : 'views/private/badge/add.html',
        controller :'BadgeController',
        title: 'Manage title - Add new',
        access: { requiredLogin: true }
    })
    .state('system-badge-edit',{
        url: "/system/badges/edit/:id",
        templateUrl : 'views/private/badge/edit.html',
        controller :'BadgeController',
        title: 'Manage title - Edit',
        access: { requiredLogin: true }
    })
    /*===============USERS================*/
      .state('users', {
        url: "/list-users.html",
        templateUrl : 'views/public/user/list.html',
        controller :'ListUserController',
        title: 'List members',
        access: { requiredLogin: false }
    })
    .state('profile', {
        url: "/user-profile/:id/:slug",
        templateUrl : 'views/public/user/profile.html',
        controller :'ProfileUserController',
        title: 'User Profile',
        access: { requiredLogin: false }
    })
    .state('forgot_password', {
        url: "/member/forgot-password.html",
        templateUrl : 'views/public/user/resetpass.html',
        controller :'UserController',
        title: 'Forgot Password',
        access: { requiredLogin: false }
    })
    .state('reset-password', {
        url: "/users/reset-password/:token",
        templateUrl : 'views/public/user/form-reset.html',
        controller :'resetPasswordController',
        title: 'Reset Password',
        access: { requiredLogin: false }
    })
    .state('edit-user', {
        url: "/edit-profile/:id/:slug", //https://github.com/dodo/node-slug
        templateUrl : 'views/public/user/edit.html',
        controller :'EditProfile',
        title: 'Edit Profile',
        access: { requiredLogin: true }
    })
    .state('register', {
        url: "/register.html",
        templateUrl : 'views/public/register.html',
        controller :'UserController',
        title: 'Register',
        access: { requiredLogin: false }
    })
    .state('login', {
        url: "/login.html",
        templateUrl : 'views/public/login.html',
        controller :'UserController',
        title: 'Login',
        access: { requiredLogin: false }
    })
    .state('active_account', {
        url: "/users/active/:user_id/:token",
        templateUrl : 'views/public/notice.html',
        controller :'UserController',
        title: 'Active Account',
        access: { requiredLogin: false }
    })
    .state('system-user', {
        url: "/system/users",
        templateUrl : 'views/private/user/manage.html',
        controller :'ListUserController',
        title: 'System User',
        access: { requiredLogin: true }
    })
    .state('system-user-permission', {
        url: "/system/users/permission/:id",
        templateUrl : 'views/private/user/permission.html',
        controller :'EditProfile',
        title: 'Edit Profile',
        access: { requiredLogin: true }
    })
    /*================SYSTEM================*/
    .state('system', {
        url: "/system",
        templateUrl : 'views/private/system.html',
        controller :'MainController',
        title: 'System',
        access: { requiredLogin: true },
        /*isadmin:{requireAdmin: true}*/
    })
    /*===============TAGS================*/
    .state('tags', {
      url: "/tags.html",
      templateUrl: "views/public/tag/index.html",
      controller: 'ListTagController',
      title: 'Tags',
      access: { requiredLogin: false }
    })
    .state('questions_tag', {
      url: "/questions-tag/:id/:slug",
      templateUrl: "views/public/tag/question.html",
      controller: 'getQuestionByTagController',
      title: 'Tagged Question',
      access: { requiredLogin: false }
    })
    .state('system-tag',{
        url: "/system/tags",
        templateUrl : 'views/private/tag/manage.html',
        controller :'ListTagController',
        title: 'System Tags',
        access: { requiredLogin: true }
    })
    .state('system-tag-edit',{
        url: "/system/tags/edit/:id",
        templateUrl : 'views/private/tag/edit.html',
        controller :'TagDetail',
        title: 'Edit Tags',
        access: { requiredLogin: true }
    })
    /*================SYSTEM SETTING==============*/
    .state('system-setting',{
        url: "/system/setting",
        templateUrl : 'views/private/setting/index.html',
        controller :'SystemController',
        title: 'System Settings',
        access: { requiredLogin: true }
    })
    .state('system-setting-about',{
        url: "/system/setting/about",
        templateUrl : 'views/private/setting/about.html',
        controller :'SystemController',
        title: 'Edit recommend',
        access: { requiredLogin: true }
    })
    .state('system-setting-policy',{
        url: "/system/setting/policy",
        templateUrl : 'views/private/setting/policy.html',
        controller :'SystemController',
        title: 'Edit rules & terms',
        access: { requiredLogin: true }
    })
    .state('system-setting-help',{
        url: "/system/setting/help",
        templateUrl : 'views/private/setting/help.html',
        controller :'SystemController',
        title: 'Editing guide',
        access: { requiredLogin: true }
    })
    .state('system-report',{
        url: "/system/report",
        templateUrl : 'views/private/report/manage.html',
        controller :'ReportController',
        title: 'Manage violation',
        access: { requiredLogin: true }
    })
    /*===============404 NOT FOUND================*/
    .state('404', {
        url: "/404.html",
        templateUrl : 'views/public/404.html',
        title: '404 - Page not found',
        access: { requiredLogin: false }
    });
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
}]);
