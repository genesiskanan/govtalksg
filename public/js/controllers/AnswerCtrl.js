angular.module('AnswerCtrl',[])

.controller('AnswerController',['$scope','$rootScope','$cookieStore','$location', '$http','flash','$modal','appAlert', 'Answer','Notifi','socket',
 function($scope,$rootScope,$cookieStore,$location, $http,flash,$modal,appAlert, Answer,Notifi,socket) {

    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.entryLimit = 10;
    $http.get('api/answer/getAll')
        .success(function(list){
            $rootScope.listAnswer=list;
        })
        .error(function(){
            console.log('error');
        });
    $scope.deleteAnswer = function(id, path) {
        appAlert.confirm({title:"Erase",message:"You sure you want to delete this answer ?"},function(isOk){
            if(isOk){
                Answer.delete(id)
                    /*If successful, it reloads delete data*/
                    .success(function(data) {
                        flash.success="Clear success!";
                         $http.get('api/answer/getAll')
                            .success(function(list){
                                $scope.listAnswer=list;
                                $location.path(path);
                            })
                            .error(function(){
                                console.log('error');
                            });
                            socket.emit('total answer');
                            socket.emit('new answer');
                    });
                }
            });
        };
        $scope.acepted=function(id){
            appAlert.confirm({title:"Confirm",message:"Are you sure you want to mark this answer is correct?"+
                "You only can correct 1 answer at a time"},function(isOk){
                if(isOk){
                    $http.get('api/answer/acept/'+id)
                        .success(function(data){
                            $scope.listAnswerQuestion=data;
                            $http.get('api/question/detail/'+ data[0].questionId)
                                .success(function(data){
                                    if(!data.status)
                                        $state.go("404");
                                    $scope.questionDetail=data;
                                });
                        });
                    flash.success = "You've marked the exact answer!";
                    socket.emit('new answer');
                }
            });
        };
        $scope.listAllVote=[];
        $http.get('api/user/vote/all').success(function(all){$scope.listAllVote=all;}).error(function(){console.log('error');});
        $scope.voteUp=function(id, question_id){
            $http.get('/loggedin').success(function(isLogin){
                if(isLogin!=='0'){
                    $http.get('api/answer/vote_up/'+id)
                        .success(function(data){
                            if(parseInt(data)==1)
                                flash.success="Did you like this answer to be removed!";
                            else{
                                $http.get('api/answer/detail/'+ id)
                                .success(function(data){
                                    Notifi.create({userRecive:data.userId._id,
                                        userSend:$cookieStore.get('currentUser')._id,
                                        content:$cookieStore.get('currentUser').displayName+' Liked answers to questions '+data.questionId.title,
                                        questionId:data.questionId._id});
                                    socket.emit('voteup',{userSendName:$cookieStore.get('currentUser').displayName,
                                        userReciveId:data.userId._id,
                                        userTitle:data.questionId.title,
                                        questionIds:data.questionId._id});
                                })
                                .error(function(){
                                console.log("error");
                                });
                                flash.success="You liked this answer!";
                            }
                            $http.get('api/findAnswers/'+ question_id)
                                .success(function(data){
                                  $scope.listAnswerQuestion=data;
                                })
                                .error(function(){
                                console.log("error");
                            });
                            $http.get('api/user/vote')
                                .success(function(vote){
                                    $scope.listVote=vote;
                                })
                                .error(function() {
                                    console.log('error');
                                });
                            $http.get('api/user/vote/all').success(function(all){$scope.listAllVote=all;}).error(function(){console.log('error');});
                        })
                        .error(function(){
                            console.log('error');
                        });
                }

                else{
                    flash.error='You must login to vote !';
                }
            });
        };
        $scope.voteDown=function(id,question_id){
            $http.get('/loggedin').success(function(isLogin){
                if(isLogin!=='0'){
                    $http.get('api/answer/vote_down/'+id)
                        .success(function(data){
                            if(parseInt(data)==1)
                                flash.success="You've dislike this answer!";
                            else
                                flash.success="You do not like this answer!";
                            $http.get('api/findAnswers/'+ question_id)
                                .success(function(data){
                                  $scope.listAnswerQuestion=data;
                                })
                                .error(function(){
                                console.log("error");
                            });
                            $http.get('api/user/vote')
                                .success(function(vote){
                                    $scope.listVote=vote;
                                })
                                .error(function() {
                                    console.log('error');
                                });
                            $http.get('api/user/vote/all').success(function(all){$scope.listAllVote=all;}).error(function(){console.log('error');});
                        })
                        .error(function(){
                          flash.error="An error in the implementation process polls. Please try again later!";
                        });
                }

                else{
                    flash.error='You must login to vote !';
                }
            });
        };
        $scope.reportAnswer = function(id){
            $http.get('/loggedin').success(function(data){
                if(data!=='0'){
                    appAlert.confirm({title:"Confirm",message:"You sure you want to report this answer as a infringement?"},function(isOk){
                        if(isOk){
                            Answer.report(id)
                                .success(function(data){
                                    if(data.reported)
                                        flash.error= "You have reported this answer infringement!";
                                    else
                                        flash.success="Report Abuse success!";
                                })
                                .error(function(){
                                    console.log('error');
                                });
                        }
                    });
                }
                else
                    flash.error = "You need to log in to report a violation";
            });
    };
}])
.controller('AnswerDetail',['$scope','$http', '$stateParams', function ($scope,$http, $stateParams) {
    $http.get('api/answer/detail/'+$stateParams.id)
        .success(function(data){
            $scope.answer=data;
        })
        .error(function(){
            console.log('error');
        });
}])

.controller('CountAnswerController',['$scope','socket','$rootScope','$http', 'Answer', 
    function($scope,socket,$rootScope,$http, Answer) {
    /*Count the number of responses in the system*/
    $http.get('api/answer/count')
    .success(function(data){
        $rootScope.countAnswer=data;
    })
    .error(function(){
        console.log("error");
    });
    socket.on('total answer',function()
    {
        $http.get('api/answer/count')
        .success(function(data){
            $rootScope.countAnswer=data;
        })
        .error(function(){
            console.log("error");
        });
    });
}]);
