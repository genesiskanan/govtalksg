angular.module('UserCtrl',[])
        .controller('UserController',['$scope', '$state', '$rootScope','$cookieStore','$window', '$http','$location','$stateParams','flash','$modal','appAlert', 'User','AuthenticationService','socket', function($scope, $state, $rootScope,$cookieStore,$window, $http,$location,$stateParams,flash,$modal,appAlert, User,AuthenticationService, socket) {
        //If the user has logged on , the k allowed in the site log in or register.
        /*if($location.path().substr(0, $location.path().length)=="/login" ||
          $location.path().substr(0, $location.path().length)=="/register"
          ){
            $http.get('/loggedin').success(function(data){
              if(data!=="0")
              {
                flash.error="You actually log in!";
                $location.path('/');
              }
            });
          }*/

        /*Variable data storage form*/
        $scope.userData = {};
         $scope.login = function() {
          $scope.Proccess=true;
          /*Test data is empty*/
          if (!$.isEmptyObject($scope.userData)) {
                  $rootScope.successMsg='';

                  User.getUserbyEmail($scope.userData)
                  .success(function(u){
                    if(u.length>0){
                      if(u[0].status===0){
                        flash.error = 'Accounts not enabled , please check your email and activate your account!';
                              $scope.Proccess = false;
                              $state.go("login");
                      }
                      else{
                        User.login($scope.userData)
                        .success(function(data){
                                 $cookieStore.put('currentUser',data);
                                 $rootScope.currentUser=$cookieStore.get('currentUser');
                                 AuthenticationService.isLogged = true;
                                 $window.sessionStorage.token = data.token;
                                 flash.success="Welcome "+$cookieStore.get('currentUser').displayName+" come back!";
                                 socket.emit('add user', {username: $cookieStore.get('currentUser').displayName, avatar: $cookieStore.get('currentUser').avatar, _id: $cookieStore.get('currentUser')._id});

                                if($rootScope.oldState!==''&& $rootScope.oldState!=='active_account'){
                                  if($rootScope.oldParam.id!==null)
                                    $state.go($rootScope.oldState,{id: $rootScope.oldParam.id });
                                  else
                                    $state.go($rootScope.oldState);
                                }
                                else
                                  $state.go("home");
                        })
                         .error(function(){
                              flash.error = 'Email or password is incorrect.';
                              $scope.Proccess = false;
                              $state.go("login");
                            });
                     }
                    }
                    else
                    {
                       flash.error = 'Email or password is incorrect.';
                        $scope.Proccess = false;
                        $state.go("login");
                    }

                  });
          }
        };
        $scope.signup = function() {
                $scope.Proccess=true;
                /*Test data is empty*/
                if (!$.isEmptyObject($scope.userData)) {
                        User.signup($scope.userData)
                        .success(function(data){
                           flash.success="Sign Up Success. Please check your email to activate your account!";
                              $state.go("home");
                        })
                         .error(function(){
                              flash.error = 'This e-mail is used, select another email .';
                              $scope.Proccess = false;
                              $state.go("register");
                            });
                }
        };
        $scope.logout = function(){
                User.logout()
                        .success(function(){
                            $rootScope.currentUser=null;
                            $cookieStore.remove('currentUser');
                            if (AuthenticationService.isLogged) {
                                AuthenticationService.isLogged = false;
                                delete $window.sessionStorage.token;
                              }
                            flash.success="Logout successful!";
                            socket.emit('logout');
                        });
        };
        $scope.deleteUser = function(id,path) {
        appAlert.confirm({title:"Erase",message:"You sure you want to delete this user?"},function(isOk){
            if(isOk){
                User.delete(id)
                    /*If successful, it reloads delete data*/
                    .success(function(data) {
                      if(data.error_msg!==null){
                        flash.error=data.error_msg;
                      }
                      else{
                            flash.success="Remove members succeed!";
                            $scope.allUser=data;
                            $location.path(path);
                          }
                    });
            }
            });
        };

        $scope.resetPass = function(){
          $scope.Proccess=true;
          if (!$.isEmptyObject($scope.userData)) {
              User.forgot($scope.userData)
              .success(function(data){
                 flash.success="System recovery information sent via email to your account, please check your email.";
                 $scope.Proccess=false;
                  $state.go("home");
              });
          }
        };
}])
.controller('ActiveAccount',['$scope', '$state', 'User', function ($scope, $state, User) {
      User.activeAccount()
        .success(function(data){
          if(data.status==1){
            $scope.successMsg = true;
            $scope.Message="Account activation success! Please log in to the system to use the member functions.";
          }
          else{
            $scope.successMsg = false;
            $scope.Message="Account activation failed. Invalid token code!";
          }
        })
        .error(function() {
          console.log('error');
        });
}])
.controller('ListUserController',['$scope', 'User', 'Badge','$mdDialog', function($scope, User, Badge, $mdDialog) {
     /*Get the entire members*/
     $scope.loading=true;
     $scope.allUser=[];
     $scope.listBadge=[];
     User.get()
        .success(function(data){
          $scope.allUser=data;
        })
        .error(function(){
          console.log('error');
        });
      Badge.get()
      .success(function(badge){
        $scope.listBadge=badge;
      })
      .error(function() {
        console.log('error');
      });
      $scope.loading=false;
}])
.controller('ProfileUserController',['appAlert','flash','Restangular','$modal','$scope','$rootScope','$http','$state','$stateParams', 'User','Badge', function(appAlert,flash,Restangular,$modal,$scope,$rootScope,$http,$state,$stateParams, User,Badge) {
      /*Get the information of one member _id*/
      $scope.userInfo=[];
      User.getUserDetail()
        .success(function(data){
          $scope.userInfo=data;
        })
        .error(function(){
            console.log('error');
        });

      Badge.get()
        .success(function(data){
          $scope.listBadge=data;
        })
        .error(function() {
          console.log('error');
        });
      /*Count the number of questions that this member has posted*/
      User.countQuestion()
        .success(function(data){
          $scope.numberQuestion=data;
        })
        .error(function() {
          console.log('error');
        });
      /*Count the number of answers*/
      User.countAnswer()
        .success(function(data){
          $scope.numberAnswer=data;
        })
        .error(function() {
          console.log('error');
        });
        $http.get('api/question/getQuestionByUser/'+$stateParams.id)
          .success(function(data){
            $scope.listQuestion=data;
          });
        $http.get('api/answer/getAnswerByUser/'+$stateParams.id)
          .success(function(data){
            $scope.listAnswer=data;
          });
        $scope.currentPage = 1; /*//current page*/
        $scope.maxSize = 5; /*//pagination max size*/
        $scope.entryLimit = 5; /*//max rows for data table*/
}])
.controller('CountUserController',['$scope','User', function($scope,User) {
    /*Count the number of members in the system*/
    User.countUser()
    .success(function(data){
        $scope.countUser=data;
    })
    .error(function(){
        console.log("Request Failed");
    });
}])
.controller('EditProfile',['appAlert','flash','Restangular','$modal','$scope','$rootScope','$http','$state', 'User', 
  function(appAlert,flash,Restangular,$modal,$scope,$rootScope,$http,$state, User) {

  User.getUserDetail()
        .success(function(data){
          $scope.userAvatar=data.avatar;
          $scope.formData=data;
        })
        .error(function(){
            console.log('error');
        });
  $scope.uploadImage=function () {
    //begin modal
    var modalInstance = $modal.open({
      templateUrl: '/views/modal/upload-image.html',
      controller: 'modal.uploadImage',
      resolve: {
        /*Do not put anything up modal data in the controller is only 2 service
        items: function () {
         return $scope.items;
        }*/
      }
    });
    modalInstance.result.then(function (dataFromOkModal) {
      $scope.newAvatar=dataFromOkModal;
      //no cache url, range of random floating point numbers with up to 16 decimal places
      $rootScope.currentUser.avatar='uploads/users/'+dataFromOkModal+"?"+Math.random()*16+ new Date().getTime();
      $http.get('api/user/edit/avatar/'+dataFromOkModal)
        .success(function(data){
            $scope.userAvatar=data.avatar;
        })
        .error(function(){
          console.log('error');
        });
      console.log(dataFromOkModal);
    }, function (dataFromDissmissModal) {
      console.log(dataFromDissmissModal);
    });
    /*end modal*/
};
  $scope.updateUser= function(){
    $scope.Proccess=true;
    /*Test data is empty*/
    if (!$.isEmptyObject($scope.formData)) {
      User.edit($scope.formData)
        .success(function(data){
            $scope.formData=data;
            flash.success="Update successful!";
            $scope.Proccess = false;
        })
       .error(function(){
          flash.error = 'There is an error in the process of changing information.';
          $scope.Proccess = false;
        });
    }
  };
  $scope.changePassword = function(){
    $scope.Proccess=true;
    if(!$.isEmptyObject($scope.formData)){
      User.changePassword($scope.formData)
        .success(function(data){
          $scope.formData={};
          /*$scope.form.$setPristine();*/
          flash.success="Successful password change!";
          $scope.Proccess = false;
        })
        .error(function(){
          flash.error="Current password is incorrect, please check back!";
          $scope.Proccess = false;
        });
    }
  };
  $scope.updatePermission = function(){
    $scope.Proccess=true;
    if(!$.isEmptyObject($scope.formData)){
      User.updatePermission($scope.formData)
      .success(function(data){
          $scope.formData={};
          /*$scope.form.$setPristine();*/
          flash.success="Update role for member success!";
          $scope.Proccess = false;
          $state.go("system-user");
        })
        .error(function(){
          flash.error="An error in the processing . Please try again later!";
          $scope.Proccess = false;
        });
    }
  };


}])
.controller('resetPasswordController',['flash','$window','$scope','$rootScope','$http','$state', '$stateParams','$cookieStore', 'User','AuthenticationService',
 function(flash,$window,$scope,$rootScope,$http,$state, $stateParams,$cookieStore, User,AuthenticationService) {
  $scope.formData={};
  $scope.formData.token= $stateParams.token;
  $scope.resetPassword = function(){
    $scope.Proccess=true;
    console.log($scope.formData);
    if(!$.isEmptyObject($scope.formData)){
      User.resetPassword($scope.formData)
        .success(function(data){
          console.log(data);
            $scope.formData={};
            $scope.Proccess = false;
            if(data.error_msg!==null){
              flash.error=data.error_msg;
            }
            else{
              flash.success="Successful password change!";
              $cookieStore.put('currentUser',data);
               $rootScope.currentUser=$cookieStore.get('currentUser');
               AuthenticationService.isLogged = true;
               $window.sessionStorage.token = data.token;
              $state.go("home");
            }
        })
        .error(function(){
          flash.error="There was an error processing the request. Please perform again at another time!";
          $scope.Proccess = false;
        });
    }
  };

}]);