angular.module('NotifiCtrl',[])

.controller('NotifiController',['$scope','$cookieStore','$window','$state', '$location','$http','$rootScope','appAlert','flash','AuthenticationService','socket','Notifi',
  function($scope,$cookieStore,$window,$state, $location,$http,$rootScope,appAlert,flash,AuthenticationService,socket,Notifi) {

    $scope.updatecount=function()
    {
        $http.get('api/notifi/update').success(function(data)
        {
            $http.get('api/notifi/count/'+$cookieStore.get('currentUser')._id)
            .success(function(data)
            {
                $scope.sfc=data;
            });
        })
        .error(function()
        {
            console.log("error");
        });
    }
    // Handling Socket
    // Vote Up
    socket.on('voteup',function(data)
    {
        if($cookieStore.get('currentUser')._id == data.userReciveId)
        {
            flash.success="<a href='cau-hoi/chi-tiet/"+data.questionIds+"/'>"+data.userSendName+" Liked Dataset "+data.userTitle+"</a>";
        }
        $http.get('api/notifi/detail/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.listNotifi=data;
        });
        $http.get('api/notifi/count/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.sfc=data;
        });
    });
    // createAnswer
    socket.on('createAnswer',function(data)
    {
                if($cookieStore.get('currentUser')._id == data.userReciveId)
                {
                    flash.success=data.userSendName+" contribute to dataset "+data.userTitle;
                }
                $http.get('api/notifi/detail/' + $cookieStore.get('currentUser')._id)
                .success(function(data)
                {
                    $scope.listNotifi=data;
                });
                $http.get('api/notifi/count/' + $cookieStore.get('currentUser')._id)
                .success(function(data)
                {
                    $scope.sfc=data;
                });
    });
    // reportQuestion
    socket.on('reportQuestion',function(data)
    {
        var listAD=[];
        $http.get('api/admin')
        .success(function(listAM)
        {
            for(var i in listAM)
            {
                var item=listAM[i];
                if(listAD.indexOf(item._id)==-1)
                    listAD.push(item._id);
            }
            for(var i in listAD)
            {
                var item=listAD[i];
                if($cookieStore.get('currentUser')._id == item)
                {
                    flash.success="<a href='cau-hoi/chi-tiet/"+data.questionIds+"/'>"+data.userSendName+" reported violations dataset "+data.userTitle+"</a>";
                }
            }
        });
        $http.get('api/notifi/detail/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.listNotifi=data;
        });
        $http.get('api/notifi/count/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.sfc=data;
        });
    });
    // Browse the dataset
    socket.on('approve',function(data)
    {
        if($cookieStore.get('currentUser')._id == data.userReciveId)
        {
            flash.success="<a href='cau-hoi/chi-tiet/"+data.questionIds+"/'>The dataset "+ data.userTitle +" was Managing Published!</a>";
        }
        $http.get('api/notifi/detail/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.listNotifi=data;
        });
        $http.get('api/notifi/count/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.sfc=data;
        });
    });
    // Favorite
    socket.on('Favorite',function(data)
    {
        if($cookieStore.get('currentUser')._id == data.userReciveId)
        {
            flash.success="<a href='cau-hoi/chi-tiet/"+data.questionIds+"/'>"+data.userSendName+" tracked dataset "+data.userTitle+"</a>";
        }
        $http.get('api/notifi/detail/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.listNotifi=data;
        });
        $http.get('api/notifi/count/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.sfc=data;
        });
    });
    // delete dataset
    socket.on('deleteQuestion',function(data)
    {
        if($cookieStore.get('currentUser')._id == data.userReciveId)
        {
            flash.error="Dataset "+ data.userTitle +" was deleted!";
        }
        $http.get('api/notifi/detail/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.listNotifi=data;
        });
        $http.get('api/notifi/count/'+$cookieStore.get('currentUser')._id)
        .success(function(data)
        {
            $scope.sfc=data;
        });
    });
    // Get Notification
    $http.get('api/notifi/detail/'+$cookieStore.get('currentUser')._id)
    .success(function(data)
    {
        $scope.listNotifi=data;
    });
    $http.get('api/notifi/count/'+$cookieStore.get('currentUser')._id)
    .success(function(data)
    {
        $scope.sfc=data;
    });
}]);
