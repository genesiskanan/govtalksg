angular.module('BadgeCtrl',[])

.controller('BadgeController',['$scope','$rootScope','$cookieStore','$state', '$http','flash','$modal','appAlert', 'Badge', 
    function($scope,$rootScope,$cookieStore,$state, $http,flash,$modal,appAlert, Badge) {
    $scope.loading=true;
    Badge.get()
        .success(function(data){
            $scope.listBadge=data;
            $scope.loading=false;
        })
        .error(function() {
            console.log('error');
        });
    $scope.createBadge= function() {
            $scope.Proccess=true;
            /*Check data form if it does not do anything*/
            if (!$.isEmptyObject($scope.formData)) {

                    /*call to create the service*/
                    Badge.create($scope.formData)

                            /*if successful, get more new data to new*/
                            .success(function(data) {
                                    $scope.formData = {};/* // Clear form*/
                                    $scope.form.$setPristine();
                                    $scope.Proccess=false;
                                    flash.success="Add new success!";
                                    $state.go('system-badge');

                            })
                            .error(function() {
                                flash.error="This title was used.";
                                $scope.Proccess=false;
                            });
            }
            else{
                    flash.error="You need to fill the position .";
                    $scope.Proccess=false;
            }
    };
    $scope.deleteBadge = function(id) {
        appAlert.confirm({title:"erase",message:"You sure you want to delete this title?"},function(isOk){
        if(isOk){
            Badge.delete(id)
                /*If successful, it reloads delete data*/
                .success(function(data) {
                        $scope.listBadge = data;
                        flash.success="Clear success!";
                })
                .error(function() {
                    flash.error="An error has occurred . Delete failed!";
                });
        }
    });
    };
}])
.controller('BadgeDetail',['$scope','$http','$state', '$stateParams','flash', 'Badge', function ($scope,$http,$state, $stateParams,flash, Badge) {
    $http.get('api/badge/detail/'+$stateParams.id)
        .success(function(data){
            $scope.formData=data;
        })
        .error(function(){
            console.log('error');
        });
    $scope.updateBadge= function() {
            $scope.Proccess=true;
            /*Check data form if it does not do anything*/
            if (!$.isEmptyObject($scope.formData)) {

                    /*call to create the service*/
                    Badge.edit($scope.formData)
                        .success(function(data) {
                                $scope.formData = {};
                                $scope.form.$setPristine();
                                $scope.Proccess=false;
                                flash.success="Update successful!";
                                $state.go('system-badge');

                        })
                        .error(function() {
                            flash.error="This title was used.";
                            $scope.Proccess=false;
                        });
            }
            else{
                    flash.error="An adequate fill position.";
                    $scope.Proccess=false;
            }
    };
}]);
