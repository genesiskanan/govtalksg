angular.module('TagCtrl',[])

.controller('TagController',['$scope','$cookieStore','$http','$location','$rootScope','flash','$modal','appAlert','Tag', function($scope,$cookieStore,$http,$location,$rootScope,flash,$modal,appAlert,Tag) {

    $http.get('api/tag/count')
        .success(function(data){
            $scope.totalItems=data;
            console.log(data);
        });
    $scope.currentPage = 1; /*//current page*/
    $scope.maxSize = 5; /*//pagination max size*/
    $scope.entryLimit = 8; /*//max rows for data table*/
}])
.controller('ListTagController',['$scope','Tag','socket','flash','appAlert', function($scope,Tag,socket,flash,appAlert) {
    /*Get the whole tags*/
    $scope.loading=true;
    $scope.allTag=[];
    Tag.get()
        .success(function(data){
            $scope.allTag=data;
            $scope.loading=false;
        })
        .error(function(){
            console.log('Request Failed');
        });
        /*Variable data storage form*/
    $scope.formData = {};

    /*When pressing the submit form will send the data api/tag*/
    $scope.createTag= function() {
            $scope.Proccess=true;
            /*Check data form empty if it does not do anything*/
            if (!$.isEmptyObject($scope.formData)) {

                    /*call to create the service*/
                    Tag.create($scope.formData)

                            /*if successful, get more new data to new*/
                            .success(function(data) {
                                    $scope.formData = {}; /*// Erase form*/
                                    $scope.form.$setPristine();
                                    $scope.Proccess=false;
                                    $('.add-tag-box').modal('hide');
                                    flash.success="Added new tag success!";
                                    $scope.allTag = data;
                            })
                            .error(function() {
                                flash.error="This tag is used.";
                                $scope.Proccess=false;
                            });
                            socket.emit('total tag');
            }
            else{
                    flash.error="You need to fill the portion .";
                    $scope.Proccess=false;
            }
    };
    $scope.deleteTag = function(id) {
        appAlert.confirm({title:"Erase",message:"You sure you want to remove this tag ?"},function(isOk){
        if(isOk){
            Tag.delete(id)
                /*If successful, it reloads delete data*/
                .success(function(data) {
                        $scope.allTag = data;
                        flash.success="Deleted successfully!";
                });
                socket.emit('total tag');
        }
    });
    };
}])
.controller('PopularTagController',['$scope','Tag', function($scope,Tag) {
    /*Get popular tags*/
    $scope.popularTag=[];
    Tag.popularTag()
    .success(function(data){
      $scope.popularTag=data;
    })
    .error(function(){
      console.log('Request Failed');
    });
}])
.controller('getQuestionByTagController',['$scope','$stateParams','$http', function($scope,$stateParams,$http) {
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.entryLimit = 10;
    var tag_id =$stateParams.id;
    $http.get('/api/getQuestionByTag/'+tag_id)
    .success(function(data){
        $scope.listQuestion=data;
    })
    .error(function(){
        console.log("error");
    });
    $http.get('/api/tag/getTagById/'+tag_id)
    .success(function(data){
        $scope.tag=data[0].tagName;
    })
    .error(function(){
        console.log("error");
    });
}])
.controller('AutocompleteTagController',['$scope','DataAccess', '$http', function($scope,DataAccess, $http) {
    /*Get a list of existing tags, for autocomplete*/
    DataAccess.loadTag('tags');

    $scope.loadItems = function($query) {
        return DataAccess.searchTag('tags', $query);
    };
  var maxTags = 3;

  $scope.$watch('formData.tag.length', function(value) {
    if (value < maxTags)
      $scope.placeholder = 'Add up ' + (maxTags - value) + ' tags';
    else
        if(value>0)
            $scope.placeholder='Edit tag';
        else
            $scope.placeholder = 'more';

  });
}])
.controller('TagDetail',['$scope','$http','$state', '$stateParams','flash', 'Tag', function ($scope,$http,$state, $stateParams,flash, Tag) {
    $http.get('api/tag/detail/'+$stateParams.id)
        .success(function(data){
            $scope.formData=data;
            console.log(data);
        })
        .error(function(){
            console.log('error');
        });
    $scope.updateTag= function() {
            $scope.Proccess=true;
            /*Check data form empty, if it does not do anything*/
            if (!$.isEmptyObject($scope.formData)) {

                    /*call to create the service*/
                    Tag.edit($scope.formData)
                        .success(function(data) {
                                $scope.formData = {};
                                $scope.form.$setPristine();
                                $scope.Proccess=false;
                                flash.success="Update successful!";
                                $state.go('system-tag');

                        })
                        .error(function() {
                            flash.error="This tag is used.";
                            $scope.Proccess=false;
                        });
            }
            else{
                    flash.error="You need to fill the portion.";
                    $scope.Proccess=false;
            }
    };
}])
.controller('CountTagController',['$scope','$rootScope','socket','$http', 'Tag', function($scope,$rootScope,socket,$http, Tag) {
    /*Count the number of questions in the system*/
    $http.get('api/tag/count')
    .success(function(data){
        $rootScope.countTag=data;
    })
    .error(function(){
        console.log("error");
    });
    socket.on('total tag',function()
    {
        $http.get('api/tag/count')
        .success(function(data){
            $rootScope.countTag=data;
        })
        .error(function(){
            console.log("error");
        });
    });
}]);
