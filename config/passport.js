
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
var randtoken        = require('rand-token');

// load model User
var User       = require('../app/models/user');

// Initialize variable authentication
var configAuth = require('./auth');

module.exports = function(passport) {

    //used to create the session
    passport.serializeUser(function(user, done) {
        return done(null, user.id);
    });

    // used to cancel the session
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            return done(err, user);
        });
    });

    // =========================================================================
    // SIGN THROUGH THE SYSTEMS ======================================================
    // =========================================================================
    passport.use('login', new LocalStrategy({
        // Passport uses the default username to login. Here use email instead
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows user to route calls to check the log or not
    },
    function(req, email, password, done) {

        // asynchronous
        process.nextTick(function() {
            User.findOne({ 'email' :  email }, function(err, user) {
                // if there is an error, it returns an error
                if (err) {
                    return done(err);
                }

                if (!user || !user.validPassword(password)) {
                    return done(null, false);
                }
                // the successful implementation of the returns user information
                else {
                    return done(null, user);
                }
            });
        });
    }));

    // =========================================================================
    // SIGN UP ======================================================
    // =========================================================================

    passport.use('signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, email, password, done) {

        process.nextTick(function() {

            // check email was used or not
            User.findOne({'email': email}, function(err, existingUser) {

                if (err) {
                    return done(err);
                }

                // if a user exists with this email
                if (existingUser) {
                    return done(null, false);
                }
                //  Otherwise, create a new user
                else {

                    var newUser         = new User();
                    newUser.displayName = req.body.displayName;
                    newUser.email       = email;
                    newUser.password    = newUser.generateHash(password);
                    newUser.avatar      = 'images/default.png';
                    newUser.role        = 'user';
                    newUser.activeToken = randtoken.generate(60);

                    newUser.save(function(err) {
                        if (err) {
                            throw err;
                        }
                        return done(null, newUser);
                    });
                }
            });
        });
    }));

    // =========================================================================
    // LOGIN WITH FACEBOOK =================================================
    // =========================================================================

    passport.use(new FacebookStrategy({
        clientID          : configAuth.facebookAuth.clientID,
        clientSecret      : configAuth.facebookAuth.clientSecret,
        callbackURL       : configAuth.facebookAuth.callbackURL,
        passReqToCallback : true,
        profileFields     : ['id', 'displayName', 'photos', 'emails']

    },
    function(req, token, refreshToken, profile, done) {

        process.nextTick(function() {
            User.findOne({ 'email' : profile.emails[0].value }, function(err, user) {
                if (err) {
                    return done(err);
                }

                if (user) {
                        return done(null, user);
                }
                else {
                    // if there is no user, create them
                    var newUser         = new User();
                    newUser.displayName = profile.displayName;
                    newUser.email       = profile.emails[0].value;
                    newUser.avatar      = profile.photos[0].value;
                    newUser.role        = 'user';
                    newUser.status      = 1;
                    newUser.save(function(err) {
                        if (err) {
                            throw err;
                        }
                        return done(null, newUser);
                    });
                }
            });
        });

    }));

    // =========================================================================
    // Sign in with your Google account =========================================
    // =========================================================================

    passport.use(new GoogleStrategy({
        clientID          : configAuth.googleAuth.clientID,
        clientSecret      : configAuth.googleAuth.clientSecret,
        callbackURL       : configAuth.googleAuth.callbackURL,
        passReqToCallback : true
    },
    function(req, token, refreshToken, profile, done) {
        process.nextTick(function() {
            User.findOne({ 'email' : profile.emails[0].value }, function(err, user) {
                if (err) {
                    return done(err);
                }

                if (user) {
                    return done(null, user);
                }
                else {
                    var newUser         = new User();
                    newUser.displayName = profile.displayName;
                    newUser.email       = profile.emails[0].value;
                    newUser.avatar      = 'images/default.png';
                    newUser.role        = 'user';
                    newUser.status      = 1;
                    newUser.save(function(err) {
                        if (err) {
                            throw err;
                        }
                        return done(null, newUser);
                    });
                }
            });
        });
    }));
};
